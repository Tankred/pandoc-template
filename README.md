# pandoc-template

Pandoc html template

## Getting Started

Default pandoc template location

```
_ cp src/template.html /usr/share/pandoc-2.9.2.1/data/templates

```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

Tankred

## License

Add link to license file for details

## Acknowledgments

* Hat tip to anyone whose code was used
